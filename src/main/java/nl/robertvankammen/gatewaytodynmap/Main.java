package nl.robertvankammen.gatewaytodynmap;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkPopulateEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.dynmap.DynmapAPI;
import org.dynmap.bukkit.DynmapPlugin;

import java.util.Arrays;
import java.util.UUID;

import static org.bukkit.Material.END_GATEWAY;

public class Main extends JavaPlugin implements Listener {
    private DynmapAPI dynmapAPI;

    @Override
    public void onEnable() {
        var pluginManager = this.getServer().getPluginManager();
        pluginManager.registerEvents(this, this);
        dynmapAPI = (DynmapPlugin) pluginManager.getPlugin("dynmap");
    }

    @EventHandler
    public void onSpawn(ChunkPopulateEvent event) {
        if (event.getWorld().getName().equals("world_the_end")) {
            var endGateway = Arrays.stream(event.getChunk().getTileEntities())
                    .filter(blockState -> blockState.getBlockData().getMaterial().equals(END_GATEWAY))
                    .findAny();
            if (endGateway.isPresent()) {
                var locationGateway = endGateway.get().getLocation();
                var markerAPI = dynmapAPI.getMarkerAPI();
                var markerIcon = markerAPI.getMarkerIcon("default");
                markerAPI.getMarkerSet("markers").createMarker(UUID.randomUUID().toString(), "gateway", "world_the_end", locationGateway.getBlockX(), locationGateway.getBlockY(), locationGateway.getBlockZ(), markerIcon, true);
            }
        }
    }
}
